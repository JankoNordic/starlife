## Introduction

This project is a technical demo of a simple full-stack application. It has a succinct web UI  that is a portal for managing planets and species that are Star Wars related.

The project deals with the following concepts: ASP.NET core, CRUD, Docker, Entity framework, MVC, PostgreSQL, REST API, React, Sass, TypeScript.

Assumes that the local environment runs on the Windows operating system. Tested on Windows 10.
Caveat: Instructions don't cover all potential issues faced during building and running the app.

App's url: https://star-life.herokuapp.com/

## Local environment
1. Install Visual studio.
    * https://docs.microsoft.com/en-us/visualstudio/install/install-visual-studio?view=vs-2019
2. Install the latest versions of python3 and python2.
    * https://www.python.org/downloads/
3. Rename python.exe to python2.exe in the python2 installation folder
4. Set them to the PATH system variable on Windows and make sure that Python3's position is upper in the PATH list.
    * Test that the following commands give the expected output(python and python3 should output the latest python3 version and python2 the latest version of python2):
        > python --version
        > python2 --version
        > python3 --version
5. Test that the front end's ReactJS project is built successfully.
    * In directory: ..\starlife\ClientApp run following commands and solve possible build errors.
        > npm install
        > npm start
6. Install postgresql.
    * https://www.postgresql.org/download/windows/
7. Create a local PostgreSQL database, for instance, using pgAdmin.
    * https://www.postgresqltutorial.com/postgresql-create-database/
8. Update the file's ../starlife/appsettings.json connection strings with the information used in the database creation
    * Update the Username and Password at least if the same information wasn't used.
9. Build the project on Visual studio.
10. Run project using Visual studio's IIS Express.

## Local docker environment
1. Install Docker Desktop.
    * https://docs.docker.com/docker-for-windows/install/
2. Open PowerShell in the project directory.
3. Build the docker image.
    > docker build --pull -t starlife .
4. Run the docker container.
    > docker run --rm -it -p 8000:80 starlife
5. Navigate to http://localhost:8000/ on a web browser to see is the app working.

## Remote heroku docker environment
1. Install Heroku.
    * https://devcenter.heroku.com/articles/heroku-cli#download-and-install
2. Create a Heroku account.
    * https://signup.heroku.com/
3. Open PowerShell in the project directory.
4. Log in heroku.
    > heroku login
5. Create a Heroku app.
    > heroku create |app-name|
6. Add a Postgres database addon to the app.
    * Use Heroku's web UI: 
        1. Log in.
        2. Select the app.
        3. Select resources tab
        4. Search "Postgres"
        5. Add database addon
    * On console: 
        > heroku addons:create heroku-postgresql:hobby-dev -a |app-name|
7. Update Postgres database addon on Heroku.
    * Connection details can be seen on Heroku's web UI: App -> Resources -> Heroku Postgress Addon -> Settings -> View Credentials or using a command: heroku config:get DATABASE_URL -a |app-name|
    > dotnet ef database update --connection "Host=|Host|; Port=|Port|; User ID=|UserI ID|;Password=|Password|;Pooling=true;SSL Mode=Require;Trust Server Certificate=True;"
8. Add environmental variables to heroku.
    > heroku config:set ASPNETCORE_ENVIRONMENT=Release -a |app-name|
    > heroku config:set RUNTIME_ENVIRONMENTE=heroku_docker -a |app-name|
9. Log in to Container Registry.
    > heroku container:login
10. Build a docker image.
    * This command saves Heroku's Postgres database addon connection string to a variable and then passes it to a docker build. Should be run on powerShell
        > $url = heroku config:get DATABASE_URL -a |app-name|; docker build --build-arg DATABASE_URL=$url --pull -t starlife .
11. Tag the docker build.
    > docker tag |IMAGE ID| registry.heroku.com/|app-name|/web
12. Push the docker image to the registry.
    > docker push registry.heroku.com/|app-name|/web
13. Release the app.
    > heroku container:release web -a |app-name|

## Additional references
Heroku deployment with Docker reference: https://devcenter.heroku.com/articles/container-registry-and-runtime.

Heroku Postgres addon addition reference: https://devcenter.heroku.com/articles/heroku-postgresql#provisioning-heroku-postgres.
