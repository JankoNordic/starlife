﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StarLife.Data;
using StarLife.Models;
using Microsoft.Extensions.Logging;

namespace StarLife.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlanetsController : ControllerBase
    {
        private readonly StarLifeContext _context;
        private readonly ILogger _logger;

        public PlanetsController(ILogger<PlanetsController> logger, StarLifeContext context)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Planets
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DtoPlanet>>> GetPlanet()
        {
            //  return await _context.Planet.Include("Species").ToListAsync();
            return await _context.Planet
                .Select(c => new DtoPlanet { Id = c.Id, Name = c.Name, DtoSpecies = (ICollection<DtoSpecies>)c.Species
                .Select(p => new DtoSpecies { Id = p.Id, SwapiApiId = p.SwapiApiId } ) }).ToListAsync();
        }

        // GET: api/Planets/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DtoPlanet>> GetPlanet(int id)
        {
            var planet = await _context.Planet.Include("Species").SingleOrDefaultAsync(i => i.Id == id);

            if (planet == null)
            {
                return NotFound();
            }

            var dtoPlanet = new DtoPlanet
            {
                Id = planet.Id,
                Name = planet.Name,
                DtoSpecies = planet.Species.Select(p => new DtoSpecies { Id = p.Id, SwapiApiId = p.SwapiApiId }).ToList()
            };

            return dtoPlanet;
        }

        // PUT: api/Planets/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlanet(int id, Planet updatedPlanet)
        {
            if (id != updatedPlanet.Id)
            {
                return BadRequest();
            }


            var planet = await _context.Planet.Include("Species").SingleOrDefaultAsync(i => i.Id == updatedPlanet.Id);
            // TODO: Remove duplicate species from planet
            if (updatedPlanet.Species != null)
            {
                foreach (var species in updatedPlanet.Species.Reverse<Species>())
                {
                    var DbSpecies = await _context.Species.SingleOrDefaultAsync(i => i.SwapiApiId == species.SwapiApiId);

                    if (DbSpecies != null)
                    {
                        updatedPlanet.Species.Remove(species);
                        updatedPlanet.Species.Add(DbSpecies);
                    }
                    _logger.LogInformation(species.SwapiApiId.ToString());

                }
            }

            planet.Name = updatedPlanet.Name;
            planet.Species = updatedPlanet.Species.Distinct().ToList();

            try
            {
                await _context.SaveChangesAsync();
            }

            catch (DbUpdateConcurrencyException)
            {
                if (!PlanetExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Planets
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Planet>> PostPlanet(Planet planet)
        {
            if (planet.Species != null)
            {
                foreach (var species in planet.Species.Reverse<Species>())
                {
                    var DbSpecies = await _context.Species.SingleOrDefaultAsync(i => i.SwapiApiId == species.SwapiApiId);

                    if (DbSpecies != null)
                    {
                        planet.Species.Remove(species);
                        planet.Species.Add(DbSpecies);
                    }
                }
            }
            _context.Planet.Add(planet);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPlanet", new { id = planet.Id }, planet);
        }

        // DELETE: api/Planets/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Planet>> DeletePlanet(int id)
        {

            var planet = await _context.Planet.FindAsync(id);
            if (planet == null)
            {
                return NotFound();
            }

            _context.Entry(planet).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlanetExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            _context.Planet.RemoveRange(planet);
            await _context.SaveChangesAsync();

            return planet;
        }

        private bool PlanetExists(int id)
        {
            return _context.Planet.Any(e => e.Id == id);
        }
    }
}
