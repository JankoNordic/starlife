import React, { useState, useEffect } from 'react';

import ServerAPI from './Scripts/ServerAPI';
import SpeciesAPI from './Scripts/SpeciesAPI';
import Utility from './Scripts/Utility';

import {
  Planet, NewPlanet, PlanetsProps, Species,
} from './Types';
import styles from './Styles.module.scss';

function Planets(props: PlanetsProps) {
  const { instanceStyling = 'planets' } = props;
  const [species, setSpecies] = useState<Species[]>([]);
  const [speciesCount, setSpeciesCount] = useState(0);
  const [planets, setPlanets] = useState<Planet[]>([]);

  useEffect(
    () => {
      ServerAPI.getPlanets().then((value) => setPlanets(value));
      SpeciesAPI.getSpecies().then((value) => { setSpecies(value); });
      SpeciesAPI.getSpeciesCount().then((value) => setSpeciesCount(value));
    },
    [],
  );

  async function addSpecies(event: React.FormEvent<HTMLFormElement>, planet: Planet) {
    const updatedPlanet = planet;
    const swapiApiId = Utility.getSwapiApiIdFromUrl(event.currentTarget.speciesUrl.value);

    updatedPlanet.species.push({ swapiApiId });

    ServerAPI.updatePlanet(updatedPlanet);

    // TODO: Make updating the page work without a reload.
    // eslint-disable-next-line no-restricted-globals
    setTimeout(() => { location.reload(); }, 200);
  }

  async function deleteSpecies(planet: Planet, swapiApiId: number) {
    const updatedPlanet = planet;

    const index = updatedPlanet
      .species.findIndex((thisSpecies) => thisSpecies.swapiApiId === swapiApiId);
    updatedPlanet.species.splice(index, 1);

    ServerAPI.updatePlanet(updatedPlanet);
  }

  async function addPlanet(event: React.FormEvent<HTMLFormElement>) {
    const newPlanet: NewPlanet = { name: event.currentTarget.planetName.value };
    ServerAPI.addPlanet(newPlanet);

    // TODO: Make updating the page work without a reload.
    // eslint-disable-next-line no-restricted-globals
    setTimeout(() => { location.reload(); }, 200);
  }

  async function updatePlanetName(event: React.FormEvent<HTMLFormElement>, planet: Planet) {
    const updatedPlanet = planet;
    const updatedName = event.currentTarget.planetName.value;

    updatedPlanet.name = updatedName;

    ServerAPI.updatePlanet(updatedPlanet);

    // TODO: Make updating the page work without a reload.
    // eslint-disable-next-line no-restricted-globals
    setTimeout(() => { location.reload(); }, 200);
  }

  // TODO: Change key values.
  return (
    <div className={styles[instanceStyling]}>
      <div className={styles.title}>Star Life</div>
      {planets.map((planet) => (
        <div className={styles.planetContainer} key={Math.random()}>
          <div className={styles.planetInfoContainer} key={Math.random()}>
            <form onSubmit={(event) => {
              event.preventDefault(); updatePlanetName(event, planet);
            }}
            >
              <label htmlFor="planetName">
                Name:
                <input
                  className={styles.planetNameText}
                  type="text"
                  id="planetName"
                  name="planetName"
                  defaultValue={planet.name}
                />
              </label>
              <input type="submit" value="Update name" />
            </form>
            <div className={styles.planetIdTextQualifier}>ID: </div>
            <div className={styles.planetIdText}>{planet.id}</div>
            <button
              type="button"
              onClick={(event) => {
                ServerAPI.deletePlanet(planet.id);
                event.currentTarget.parentElement!.parentElement!.remove();
              }}
            >
              Remove planet
            </button>
          </div>
          <div className={styles.speciesInfoContainer}>
            { species.length === 0 ? null : planet.species
              .filter((element) => element.swapiApiId <= speciesCount)
              .map((element) => (
                <div className={styles.singleSpeciesInfoContainer} key={Math.random()}>
                  <div className={styles.speciesNameQualifier}>Name:</div>
                  <div>{species[element.swapiApiId - 1].name}</div>
                  <div className={styles.speciesClassificationQualifier}>Classification:</div>
                  <div>{species[element.swapiApiId - 1].classification}</div>
                  <div className={styles.speciesLanguageQualifier}>Language:</div>
                  <div>{species[element.swapiApiId - 1].language}</div>
                  <div className={styles.speciesHomeworldQualifier}>Homeworld:</div>
                  <div>{species[element.swapiApiId - 1].homeworld}</div>
                  <button
                    type="button"
                    onClick={(event) => {
                      deleteSpecies(planet, element.swapiApiId);
                      event.currentTarget.parentElement!.remove();
                    }}
                  >
                    Remove species
                  </button>
                </div>
              ))}
          </div>
          <form onSubmit={(event) => { event.preventDefault(); addSpecies(event, planet); }}>
            <select name="speciesUrl">
              {species.map((singleSpecies) => (
                <option value={singleSpecies.url} key={Math.random()}>
                  {singleSpecies.name}
                </option>
              ))}
            </select>
            <input type="submit" value="Add species " />
          </form>
        </div>
      ))}
      <div className={styles.addPlanetContainer}>
        <form onSubmit={(event) => { event.preventDefault(); addPlanet(event); }}>
          <label htmlFor="fname">
            Planet name:
            <input type="text" id="planetName" name="planetName" />
            <input type="submit" value="add planet" />
          </label>
        </form>
      </div>
    </div>
  );
}

export default Planets;
