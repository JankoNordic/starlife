import * as React from 'react';
import Planets from './Planets';

import './custom.css';

export default () => (
  <Planets />
);
