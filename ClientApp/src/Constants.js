const Constants = {
  httpMethods: {
    GET: 'GET',
    PUT: 'PUT',
    POST: 'POST',
    DELETE: 'DELETE',
  },

  serverPaths: {
    planets: 'api/Planets',
  },

  speciesApiPaths: {
    getSpecies: 'https://swapi.dev/api/species/',
  },

  abbreviation: {
    http: 'http',
    https: 'https',
  },
};
Object.freeze(Constants);

export default Constants;
