export type SwapiApiId = {
  swapiApiId: number;
};

export type DtoPlanet = {
  id: number;
  name: string;
  dtoSpecies: SwapiApiId[];
};

export type Planet = {
  id: number;
  name: string;
  species: SwapiApiId[];
};

export type NewPlanet = {
  name: string;
  species?: SwapiApiId[];
};

export type PlanetsProps = {
  instanceStyling?: string;
};

export type Species = {
  name: string;
  classification: string;
  language: string;
  homeworld: string;
  url: string;
};
