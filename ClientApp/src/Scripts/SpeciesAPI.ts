import Constants from '../Constants';
import { Species } from '../Types';

async function getSpeciesFetch(
  url: any = Constants.speciesApiPaths.getSpecies,
  species: Species[],
): Promise<Species[]> {
  let speciesThis = species;

  const response = await fetch(url, {
    method: Constants.httpMethods.GET,
  });
  const responseObject = await response.json();

  speciesThis = speciesThis.concat(responseObject.results);
  if (responseObject.next) {
    // Check and modify protocol if needed.
    if (responseObject.next.substr(0, 3) === 'http' && responseObject.next.substr(0, 4) !== 'https') {
      responseObject.next.replace(Constants.abbreviation.http, Constants.abbreviation.https);
    }

    return getSpeciesFetch(responseObject.next, speciesThis);
  }
  return speciesThis;
}

const SpeciesAPI = {
  async getSpecies(
    url: any = Constants.speciesApiPaths.getSpecies,
  ): Promise<Species[]> {
    let species: Species[] = [];

    species = await getSpeciesFetch(url, species);
    return species;
  },

  async getSpeciesCount(): Promise<number> {
    const response = await fetch(Constants.speciesApiPaths.getSpecies, {
      method: Constants.httpMethods.GET,
    });

    const responseObject = await response.json();

    return responseObject.count;
  },
};

export default SpeciesAPI;
