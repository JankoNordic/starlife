import Constants from '../Constants';
import { Planet, DtoPlanet, NewPlanet } from '../Types';

function parseDtoPlanet(planet: DtoPlanet): DtoPlanet {
  const parsedPlanet = planet;
  if (!parsedPlanet.dtoSpecies) {
    parsedPlanet.dtoSpecies = [];
  }
  return parsedPlanet;
}

function castDtoPlanetsToPlanets(planets: DtoPlanet[]): Planet[] {
  const castedPlanets: Planet[] = [];

  for (let i = 0; i < planets.length; i += 1) {
    const planet: Planet = {
      id: planets[i].id,
      name: planets[i].name,
      species: planets[i].dtoSpecies,
    };
    castedPlanets.push(planet);
  }
  return castedPlanets;
}

const serverAPI = {
  async getPlanets(): Promise<Planet[]> {
    const response = await fetch(Constants.serverPaths.planets, {
      method: Constants.httpMethods.GET,
    });
    const planets: DtoPlanet[] = await response.json();

    for (let i = 0; i < planets.length; i += 1) {
      planets[i] = parseDtoPlanet(planets[i]);
    }
    return castDtoPlanetsToPlanets(planets);
  },

  async updatePlanet(planet: Planet) {
    const url = `${Constants.serverPaths.planets}/${planet.id}`;
    fetch(url, {
      method: Constants.httpMethods.PUT,
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(planet),
    });
  },

  async addPlanet(newPlanet: NewPlanet) {
    fetch(Constants.serverPaths.planets, {
      method: Constants.httpMethods.POST,
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newPlanet),
    });
  },

  async deletePlanet(id: number) {
    const url = `${Constants.serverPaths.planets}/${id}`;
    fetch(url, {
      method: Constants.httpMethods.DELETE,
    });
  },
};

export default serverAPI;
