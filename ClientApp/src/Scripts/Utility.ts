const Utility = {
  getSwapiApiIdFromUrl(url: string): number {
    const slashIndex = url.substr(0, url.length - 1).lastIndexOf('/');
    const swapiApiId = parseInt(url.substr(0, url.length - 1).substr(slashIndex + 1), 10);
    return swapiApiId;
  },
};
export default Utility;
