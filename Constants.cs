﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarLife
{
    public class Contants
    {
        public readonly struct Runtime_environments
        {
            public const string Local = "local";
            public const string Local_docker = "local_docker";
            public const string Heroku_docker = "heroku_docker";
        }
    }
}
