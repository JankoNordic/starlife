# https://hub.docker.com/_/microsoft-dotnet
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
ARG RUNTIME_ENVIRONMENT=local_docker
ARG DATABASE_URL
RUN apt-get update -yq \
    && apt-get install curl gnupg -yq \
    && curl -sL https://deb.nodesource.com/setup_12.x | bash \
    && apt-get install nodejs -yq
WORKDIR /source

# Copy csproj and restore as distinct layers
COPY *.sln .
COPY *.csproj .
# --disable-parallel is used because of this bug: https://github.com/NuGet/Home/issues/9020
# TODO: Remove --disable-parallel if the issue is fixed or if a workaround is implemented 
# or of internet speend isn't an issue
RUN dotnet restore --disable-parallel

# Copy everything else and build app
COPY . .
RUN dotnet tool restore
RUN dotnet publish -c release -o /app --no-restore

# Final stage/image
FROM mcr.microsoft.com/dotnet/aspnet:5.0
ARG RUNTIME_ENVIRONMENT=local_docker
ENV RUNTIME_ENVIRONMENT=$RUNTIME_ENVIRONMENT
WORKDIR /app
COPY --from=build /app ./

ENTRYPOINT ["dotnet", "StarLife.dll"]

