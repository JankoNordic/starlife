﻿using Microsoft.EntityFrameworkCore;
using StarLife.Models;

namespace StarLife.Data
{
    public class StarLifeContext : DbContext
    {
        public StarLifeContext(DbContextOptions<StarLifeContext> options)
          : base(options)
        {
        }

        public DbSet<Planet> Planet { get; set; }
        public DbSet<Species> Species { get; set; }
        public DbSet<DtoSpecies> DtoSpecies { get; set; }
        public DbSet<DtoPlanet> DtoPlanet { get; set; }
    }
}
