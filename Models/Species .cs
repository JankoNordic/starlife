﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace StarLife.Models
{
    public class Species
    {
        public int Id { get; set; }
        [Required]
        public int SwapiApiId { get; set; }
        public virtual ICollection<Planet> Planets { get; set; }

    }
}
