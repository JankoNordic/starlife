﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace StarLife.Models
{
    public class Planet
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public ICollection<Species> Species { get; set; }

    }
}
