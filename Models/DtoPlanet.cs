﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace StarLife.Models
{
    public class DtoPlanet
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public ICollection<DtoSpecies> DtoSpecies { get; set; }
    }
}
