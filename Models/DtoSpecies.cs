﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;


namespace StarLife.Models
{
    public class DtoSpecies
    {
        public int Id { get; set; }
        [Required]
        public int SwapiApiId { get; set; }
    }
}
